<%@include file="header.jsp" %>

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->

<!-- BEGIN CONTAINER -->
<div class="page-container">

    <%@include file="navigation_bar.jsp" %>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.jsp">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>User Registration</span>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div class="pull-right tooltips btn btn-sm">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs">November 06 / 2017 </span>&nbsp;
                    </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> User's Details
                <small>( Insert / Update / Delete and Search )</small>
            </h1>
            <!-- END PAGE TITLE-->
            
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="icon-user font-green-haze bold"></i>
                        <span class="caption-subject bold uppercase"> Enter user's details</span>
                    </div>

                    <div class="">

                        <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                        <a href="" class=""> </a>

                    </div>

                    <div class="actions tools">
                        <a href="" class="collapse"> </a>
                        <a href="" class="reload"> </a>
                        <a href="" class="remove"> </a>
                        <a href="" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" id="name" placeholder="Name">
                                        <label>Name</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-warning">
                                        <input type="text" class="form-control" id="phoneNumber" placeholder="Contact Number">
                                        <label>Contact Number</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-success">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="email" placeholder="Email Address">
                                            <label for="form_control_1">Email</label>
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" >
                                    <span style="color: #999999">User Role</span>
                                    <div class="form-md-line-input">
                                        <select style="border-left: none" class="mt-multiselect form-control" multiple="multiple" data-label="left" data-width="100%" data-filter="true" data-action-onchange="true">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-md-line-input has-warning">
                                        <span class="font-yellow">Company</span>
                                        <select style="border-left: none" class="mt-multiselect form-control" multiple="multiple" data-label="left" data-width="100%" data-filter="true" data-action-onchange="true">
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-success">
                                        <input type="text" class="form-control" id="phoneNumber" placeholder="Contact Number">
                                        <label>City</label>  
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" id="name" placeholder="Address No">
                                        <label>Address No</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-warning">
                                        <input type="text" class="form-control" id="phoneNumber" placeholder="Street">
                                        <label>Street</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-success">
                                        <input type="text" class="form-control" id="phoneNumber" placeholder="City">
                                        <label>City</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input">
                                        <input type="text" class="form-control" id="username" placeholder="Username">
                                        <label>Username</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-warning">
                                        <input type="password" class="form-control" id="password" placeholder="Password">
                                        <label>Password</label>
                                    </div>
                                </div>
                                <div class="col-md-4" >
                                    <div class="form-group form-md-line-input has-success">
                                        <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
                                        <label>Confirm Password</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-10">
                                    <button type="button" class="btn blue">Submit</button>
                                    <button type="button" class="btn default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PAGE CONTENT-->

            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <i class="icon-user font-green-haze bold"></i>
                        <span class="caption-subject bold uppercase"> Search user details</span>
                    </div>
                    <div class="actions tools">
                        <a href="" class="collapse"> </a>
                        <a href="" class="reload"> </a>
                        <a href="" class="remove"> </a>
                        <a href="" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_2">
                        <thead>
                            <tr>
                                <th class="table-checkbox">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th> Name </th>
                                <th> User Role </th>
                                <th> Company </th>
                                <th> User Name </th>
                                <th> Password </th>
                                <th> Status </th>
                                <th> Update/Delete </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" value="1" />
                                        <span></span>
                                    </label>
                                </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                                <td> shuxer </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

        </div>
    </div>

    <!-- END CONTAINER -->

    <%@include file="footer.jsp" %>