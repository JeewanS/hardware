<%@include file="header.jsp" %>

<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->

<!-- BEGIN CONTAINER -->
<div class="page-container">

    <%@include file="navigation_bar.jsp" %>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->

            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="index.jsp">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>User Registration</span>
                    </li>
                </ul>
                <div class="page-toolbar">
                    <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                        <i class="icon-calendar"></i>&nbsp;
                        <span class="thin uppercase hidden-xs"></span>&nbsp;
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> User's Details
                <small>( Insert / Update / Delete and Search )</small>
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->

            <!-- BEGIN PAGE CONTENT-->




            <h4>Dropdown with On Change Options</h4>
            <select class="mt-multiselect btn btn-default" multiple="multiple" data-label="left" data-width="100%" data-filter="true" data-action-onchange="true">
                <option value="1">Option 1</option>
                <option value="2" selected="selected">Option 2</option>
                <!-- Option 3 will be selected in advance ... -->
                <option value="3" selected="selected">Option 3</option>
                <option value="4">Option 4</option>
                <option value="5">Option 5</option>
                <option value="6">Option 6</option>
            </select>

            <br>
            <br>
            <br>

            <button type="button" >test</button>



            <!-- END PAGE CONTENT-->
        </div>
    </div>
</div>

<!-- END CONTAINER -->

<%@include file="footer.jsp" %>
