package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <meta charset=\"utf-8\" />\n");
      out.write("        <title>Metronic Admin Theme #1 | User Login 4</title>\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\" />\n");
      out.write("        <meta content=\"Preview page of Metronic Admin Theme #1 for \" name=\"description\" />\n");
      out.write("        <meta content=\"\" name=\"author\" />\n");
      out.write("\n");
      out.write("        <!-- BEGIN GLOBAL MANDATORY STYLES -->\n");
      out.write("        <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/plugins/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/plugins/simple-line-icons/simple-line-icons.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/plugins/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <!-- END GLOBAL MANDATORY STYLES -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN PAGE LEVEL PLUGINS -->\n");
      out.write("        <link href=\"assets/global/plugins/select2/css/select2.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/plugins/select2/css/select2-bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <!-- END PAGE LEVEL PLUGINS -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN THEME GLOBAL STYLES -->\n");
      out.write("        <link href=\"assets/global/css/components.min.css\" rel=\"stylesheet\" id=\"style_components\" type=\"text/css\" />\n");
      out.write("        <link href=\"assets/global/css/plugins.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <!-- END THEME GLOBAL STYLES -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN PAGE LEVEL STYLES -->\n");
      out.write("        <link href=\"assets/pages/css/login-4.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("        <!-- END PAGE LEVEL STYLES -->\n");
      out.write("\n");
      out.write("        <link rel=\"shortcut icon\" href=\"favicon.ico\" /> </head>\n");
      out.write("    <!-- END HEAD -->\n");
      out.write("\n");
      out.write("    <body class=\" login\">\n");
      out.write("        <!-- BEGIN LOGO -->\n");
      out.write("        <div class=\"logo\">\n");
      out.write("            <img src=\"assets/images/logo.png\" width=\"180\" alt=\"\" /> \n");
      out.write("        </div>\n");
      out.write("        <!-- END LOGO -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN LOGIN -->\n");
      out.write("        <div class=\"content\">\n");
      out.write("            <!-- BEGIN LOGIN FORM -->\n");
      out.write("            <form class=\"login-form\" action=\"index.jsp\" method=\"post\">\n");
      out.write("                <h3 class=\"form-title\">Login to your account</h3>\n");
      out.write("                <div class=\"alert alert-danger display-hide\">\n");
      out.write("                    <button class=\"close\" data-close=\"alert\"></button>\n");
      out.write("                    <span> Enter any username and password. </span>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                    <label class=\"control-label visible-ie8 visible-ie9\">Username</label>\n");
      out.write("                    <div class=\"input-icon\">\n");
      out.write("                        <i class=\"fa fa-user\"></i>\n");
      out.write("                        <input class=\"form-control placeholder-no-fix\" type=\"text\" autocomplete=\"off\" placeholder=\"Username\" name=\"username\" /> </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                    <label class=\"control-label visible-ie8 visible-ie9\">Password</label>\n");
      out.write("                    <div class=\"input-icon\">\n");
      out.write("                        <i class=\"fa fa-lock\"></i>\n");
      out.write("                        <input class=\"form-control placeholder-no-fix\" type=\"password\" autocomplete=\"off\" placeholder=\"Password\" name=\"password\" /> </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-actions\">\n");
      out.write("                    <label class=\"rememberme mt-checkbox mt-checkbox-outline\">\n");
      out.write("                        <input type=\"checkbox\" name=\"remember\" value=\"1\" /> Remember me\n");
      out.write("                        <span></span>\n");
      out.write("                    </label>\n");
      out.write("                    <button type=\"submit\" class=\"btn green pull-right\">  Login </button>\n");
      out.write("                </div>\n");
      out.write("            </form>\n");
      out.write("            <!-- END LOGIN FORM -->\n");
      out.write("        </div>\n");
      out.write("        <!-- END LOGIN -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN COPYRIGHT -->\n");
      out.write("        <div class=\"copyright\"> 2017 &copy; iGroup Alliance. All rights reserved.</div>\n");
      out.write("        <!-- END COPYRIGHT -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN CORE PLUGINS -->\n");
      out.write("        <script src=\"assets/global/plugins/jquery.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/js.cookie.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/jquery.blockui.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <!-- END CORE PLUGINS -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN PAGE LEVEL PLUGINS -->\n");
      out.write("        <script src=\"assets/global/plugins/jquery-validation/js/jquery.validate.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/jquery-validation/js/additional-methods.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/select2/js/select2.full.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"assets/global/plugins/backstretch/jquery.backstretch.js\" type=\"text/javascript\"></script>\n");
      out.write("        <!-- END PAGE LEVEL PLUGINS -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN THEME GLOBAL SCRIPTS -->\n");
      out.write("        <script src=\"assets/global/scripts/app.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <!-- END THEME GLOBAL SCRIPTS -->\n");
      out.write("\n");
      out.write("        <!-- BEGIN PAGE LEVEL SCRIPTS -->\n");
      out.write("        <script src=\"assets/pages/scripts/login-4.js\" type=\"text/javascript\"></script>\n");
      out.write("        <!-- END PAGE LEVEL SCRIPTS -->\n");
      out.write("\n");
      out.write("        <script>\n");
      out.write("            $(document).ready(function ()\n");
      out.write("            {\n");
      out.write("                $('#clickmewow').click(function ()\n");
      out.write("                {\n");
      out.write("                    $('#radio1003').attr('checked', 'checked');\n");
      out.write("                });\n");
      out.write("            });\n");
      out.write("        </script>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
